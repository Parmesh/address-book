from fastapi import FastAPI, status, HTTPException
from database import Base, engine
from sqlalchemy.orm import Session
import models
import schemas

# Create the database
Base.metadata.create_all(engine)

# Initialize app
app = FastAPI()

@app.get("/")
def root():
    return "addresses"

@app.post("/address", status_code=status.HTTP_201_CREATED)
def create_address(address: schemas.Address):

    # create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # create an instance of the Address database model
    address_instance = models.Address(
        **address.dict()
    )

    # add it to the session and commit it
    session.add(address_instance)
    session.commit()
    #
    # close the session
    session.close()

    # return newly created address instance
    return address_instance

@app.get("/address/{id}")
def read_address(id: int):

    # create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # get the address with the given id
    address = session.query(models.Address).get(id)

    # close the session
    session.close()

    # check if address with given id exists. If not, raise exception and return 404 not found response
    if not address:
        raise HTTPException(status_code=404, detail="address with id {} not found".format(id))

    return address

@app.put("/address/{id}")
def update_address(id: int, task: str):

    # create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # get the address with the given id
    address = session.query(models.Address).get(id)

    # update address with the given task (if an item with the given id was found)
    if address:
        address.task = task
        session.commit()

    # close the session
    session.close()

    # check if address with given id exists. If not, raise exception and return 404 not found response
    if not address:
        raise HTTPException(status_code=404, detail="address with id {} not found".format(id))

    return address

@app.delete("/address/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_address(id: int):

    # create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # get the address with the given id
    address = session.query(models.Address).get(id)

    # if address with given id exists, delete it from the database. Otherwise raise 404 error
    if address:
        session.delete(address)
        session.commit()
        session.close()
    else:
        raise HTTPException(status_code=404, detail="address with id {} not found".format(id))

    return None

@app.get("/addresses")
def read_address_list():
    # create a new database session
    session = Session(bind=engine, expire_on_commit=False)

    # get all addresses
    address_list = session.query(models.Address).all()

    # close the session
    session.close()

    return address_list
