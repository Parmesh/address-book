from typing import Optional

from pydantic import BaseModel

# Create Address Schema (Pydantic Model)
class Address(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    mobile_number: Optional[str] = None
    address_line_1: Optional[str] = None
    address_line_2: Optional[str] = None
    landmark: Optional[str] = None
    latitude: float
    longitude: float

    class Config:
        orm_mode = True
