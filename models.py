from sqlalchemy import Column, Integer, String, Text, Numeric
from database import Base

# Define To Do class inheriting from Base
class Address(Base):
    __tablename__ = 'address'
    id = Column(Integer, primary_key=True)
    first_name = Column(String(128), nullable=True)
    last_name = Column(String(128), nullable=True)
    mobile_number = Column(String(13), nullable=True)
    address_line_1 = Column(Text(), nullable=True)
    address_line_2 = Column(Text(), nullable=True)
    landmark = Column(String(256), nullable=True)
    latitude = Column(Numeric(precision=3, scale=6, asdecimal=True))
    longitude = Column(Numeric(precision=3, scale=6, asdecimal=True))

