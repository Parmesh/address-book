# Address Book



## Getting started

1] After cloning this repository, navigate to project directory.

2] Run below commands to setup project
    
    ## virtualenv env
    
    ## source env/bin/activate
    
    ## pip install -r requirements.txt
    
    ## uvicorn main:app --reload